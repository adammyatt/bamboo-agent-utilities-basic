package com.pronetbeans.bamboo.agentutils;

import com.atlassian.bamboo.notification.AbstractNotification;
import com.atlassian.bamboo.notification.Notification;

/**
 *
 * @author Adam Myatt
 */
public class NotificationAgentUpdated extends AbstractNotification implements Notification{

    private String agentName;
    
    
    @Override
    public String getDescription() {
        return "Agent Config Updated Notification";
    }

    @Override
    public String getTextEmailContent() throws Exception {
        return "The configuration for agent " + getAgentName() + " was changed.";
    }

    @Override
    public String getHtmlEmailContent() throws Exception {
        return "The configuration for agent " + getAgentName() + " was changed.";
    }

    @Override
    public String getEmailSubject() throws Exception {
        return "Bamboo Agent Configuration Changed";
    }

    @Override
    public String getIMContent() {
        return "The configuration for agent " + getAgentName() + " was changed.";
    }

    /**
     * @return the agentName
     */
    public String getAgentName() {
        return agentName;
    }

    /**
     * @param agentName the agentName to set
     */
    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }
    
}

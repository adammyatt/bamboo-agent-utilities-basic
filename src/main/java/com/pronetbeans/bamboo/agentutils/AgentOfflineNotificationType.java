package com.pronetbeans.bamboo.agentutils;

import com.atlassian.bamboo.notification.AbstractNotificationType;
import com.atlassian.bamboo.v2.build.events.AgentOfflineEvent;

/**
 *
 * @author Adam Myatt
 */
public class AgentOfflineNotificationType extends AbstractNotificationType {

    public AgentOfflineNotificationType() {
    }

    @Override
    public boolean isNotificationRequired(@org.jetbrains.annotations.NotNull com.atlassian.event.Event event) {

        if (event instanceof AgentOfflineEvent) {
            return true;
        } else {
            return false;
        }

    }
}

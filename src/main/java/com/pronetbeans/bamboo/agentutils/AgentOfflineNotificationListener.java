package com.pronetbeans.bamboo.agentutils;

import com.atlassian.bamboo.event.AgentConfigurationUpdatedEvent;
import com.atlassian.bamboo.event.AllAgentsUpdatedEvent;
import com.atlassian.bamboo.event.HibernateEventListener;
import com.atlassian.bamboo.notification.*;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.events.AgentOfflineEvent;
import com.atlassian.event.Event;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Adam Myat
 */
public class AgentOfflineNotificationListener implements HibernateEventListener {

    private static final Logger log = Logger.getLogger(AgentOfflineNotificationListener.class);
    private NotificationManager notificationManager;
    private NotificationDispatcher notificationDispatcher;

    @Override
    public Class[] getHandledEventClasses() {
        return new Class[]{
                    AgentOfflineEvent.class, AgentConfigurationUpdatedEvent.class, AllAgentsUpdatedEvent.class
                };
    }

    @Override
    public void handleEvent(Event event) {

        if (event instanceof AgentOfflineEvent) {
            AgentOfflineEvent agentOffEvent = (AgentOfflineEvent) event;

            BuildAgent agent = agentOffEvent.getBuildAgent();

            // log.info("************ AGENT OFFLINE **************");
            // log.info("Name : " + agent.getName());
            // log.info("ID : " + agent.getId());
            // log.info("Status : " + agent.getAgentStatus());
            // log.info("Enabled : " + agent.isEnabled());
            // log.info("Is Unresponsive : " + agent.isUnresponsive());
            // log.info("Requested to be Stopped : " + agent.isRequestedToBeStopped());

            NotificationAgentOffline myNotification = new NotificationAgentOffline();
            myNotification.setAgentName(agent.getName());

            SystemNotificationService snService = new SystemNotificationServiceImpl(notificationManager);

            List<NotificationRule> rules = snService.getSystemNotificationRules();
            for (NotificationRule rule : rules) {
                NotificationType notificationType = rule.getNotificationType();
                if (notificationType instanceof AgentOfflineNotificationType) {
                    if (notificationType.isNotificationRequired(agentOffEvent)) {
                        NotificationRecipient recipient = rule.getNotificationRecipient();

                        myNotification.addRecipient(recipient);
                    }
                }
            }

            // send final notification
            notificationDispatcher.dispatchNotifications(myNotification);

        } else if (event instanceof AgentConfigurationUpdatedEvent) {
            AgentConfigurationUpdatedEvent agentEvent = (AgentConfigurationUpdatedEvent) event;

            BuildAgent agent = agentEvent.getBuildAgent();
//            log.info("************ AGENT Config Changed **************");
//            log.info("Name : " + agent.getName());
//            log.info("ID : " + agent.getId());
//            log.info("Status : " + agent.getAgentStatus());

            NotificationAgentUpdated myNotification = new NotificationAgentUpdated();
            myNotification.setAgentName(agent.getName());
            SystemNotificationService snService = new SystemNotificationServiceImpl(notificationManager);

            List<NotificationRule> rules = snService.getSystemNotificationRules();
            for (NotificationRule rule : rules) {
                NotificationType notificationType = rule.getNotificationType();
                if (notificationType instanceof AgentOfflineNotificationType) {
                    if (notificationType.isNotificationRequired(agentEvent)) {
                        NotificationRecipient recipient = rule.getNotificationRecipient();
                        myNotification.addRecipient(recipient);
                    }
                }
            }

            try {
                // send final notification
                //log.info("************ Sending Agent Config Changed Notification ************** : " + myNotification.getEmailSubject());
                notificationDispatcher.dispatchNotifications(myNotification);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (event instanceof AllAgentsUpdatedEvent) {
            AllAgentsUpdatedEvent agentEvent = (AllAgentsUpdatedEvent) event;

            //log.info("************ AGENT All Agents Updated/Changed **************");

            NotificationAgentsAllUpdated myNotification = new NotificationAgentsAllUpdated();
            SystemNotificationService snService = new SystemNotificationServiceImpl(notificationManager);

            List<NotificationRule> rules = snService.getSystemNotificationRules();
            for (NotificationRule rule : rules) {
                NotificationType notificationType = rule.getNotificationType();
                if (notificationType instanceof AgentOfflineNotificationType) {
                    if (notificationType.isNotificationRequired(agentEvent)) {
                        NotificationRecipient recipient = rule.getNotificationRecipient();
                        myNotification.addRecipient(recipient);
                    }
                }
            }

            try {
                // send final notification
                //log.info("************ Sending AGENT All Agents Updated/Changed ************** : " + myNotification.getEmailSubject());
                notificationDispatcher.dispatchNotifications(myNotification);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    public void setNotificationDispatcher(NotificationDispatcher notificationDispatcher) {
        this.notificationDispatcher = notificationDispatcher;
    }
}

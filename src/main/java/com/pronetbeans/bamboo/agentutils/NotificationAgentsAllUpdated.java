package com.pronetbeans.bamboo.agentutils;

import com.atlassian.bamboo.notification.AbstractNotification;
import com.atlassian.bamboo.notification.Notification;

/**
 *
 * @author Adam Myatt
 */
public class NotificationAgentsAllUpdated extends AbstractNotification implements Notification {

    @Override
    public String getDescription() {
        return "All Bamboo Agents Updated Notification";
    }

    @Override
    public String getTextEmailContent() throws Exception {
        return "All Bamboo agents have been updated.";
    }

    @Override
    public String getHtmlEmailContent() throws Exception {
        return "All Bamboo agents have been updated.";
    }

    @Override
    public String getEmailSubject() throws Exception {
        return "Bamboo Agents Have All Been Updated";
    }

    @Override
    public String getIMContent() {
        return "All Bamboo agents have been updated.";
    }
}

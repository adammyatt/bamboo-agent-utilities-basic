package com.pronetbeans.bamboo.agentutils;

import com.atlassian.bamboo.event.AllAgentsUpdatedEvent;
import com.atlassian.bamboo.notification.AbstractNotificationType;

/**
 *
 * @author Adam Myatt
 */
public class AgentAllUpdatedNotificationType extends AbstractNotificationType {

    public AgentAllUpdatedNotificationType() {
    }

    @Override
    public boolean isNotificationRequired(@org.jetbrains.annotations.NotNull com.atlassian.event.Event event) {

        if (event instanceof AllAgentsUpdatedEvent) {
            return true;
        } else {
            return false;
        }

    }
}

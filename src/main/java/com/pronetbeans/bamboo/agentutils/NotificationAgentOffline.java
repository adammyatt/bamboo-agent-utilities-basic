package com.pronetbeans.bamboo.agentutils;

import com.atlassian.bamboo.notification.AbstractNotification;
import com.atlassian.bamboo.notification.Notification;

/**
 *
 * @author Adam Myatt
 */
public class NotificationAgentOffline extends AbstractNotification implements Notification{

    private String agentName;
    
    
    @Override
    public String getDescription() {
        return "Agent Offline Notification";
    }

    @Override
    public String getTextEmailContent() throws Exception {
        return "The agent " + getAgentName() + " went offline.";
    }

    @Override
    public String getHtmlEmailContent() throws Exception {
        return "The agent " + getAgentName() + " went offline.";
    }

    @Override
    public String getEmailSubject() throws Exception {
        return "Bamboo Agent Offline";
    }

    @Override
    public String getIMContent() {
        return "The agent " + getAgentName() + " went offline.";
    }

    /**
     * @return the agentName
     */
    public String getAgentName() {
        return agentName;
    }

    /**
     * @param agentName the agentName to set
     */
    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }
    
}

package com.pronetbeans.bamboo.agentutils;

import com.atlassian.bamboo.event.AgentConfigurationUpdatedEvent;
import com.atlassian.bamboo.notification.AbstractNotificationType;

/**
 *
 * @author Adam Myatt
 */
public class AgentConfigUpdatedNotificationType extends AbstractNotificationType {

    public AgentConfigUpdatedNotificationType() {
    }

    @Override
    public boolean isNotificationRequired(@org.jetbrains.annotations.NotNull com.atlassian.event.Event event) {

        if (event instanceof AgentConfigurationUpdatedEvent) {
            return true;
        } else {
            return false;
        }

    }
}
